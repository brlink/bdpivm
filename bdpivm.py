#!/usr/bin/python3

# Only building software or packages in artifical environments
# is detrimental to the goals of free software. Please also
# regularily test your packages in real build environments.

# (C) Copyright 2013 Bernhard R. Link
# Redistribution and use in source and binary forms,
# with or without modification, are permitted providded
# that the following conditions are met:
# 1. Restribution of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistribution in binary form must produce the above copyright notice,
#    this list of conditions and the following disclaimer.
# 3. If the restrictions of the GNU AGPL apply to a single work containing this
#    code, that single work containing thise code must also be made available
#    under the GNU GPLv2 or not be created at all.

# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
# AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
# OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Dies ist experimenteller Code. Benutzung auf eigene Gefahr.
# Keine Haftung außer bei Vorsatz oder grober Fahrlässigkeit.

import os
import stat
import subprocess
import argparse
import platform
import time
import tempfile

##############################################################################

def o8(number):
	""" o for octet (8 bit byte), not octal ;-) """
	# optimize if you like:
	return bytes("%08x" % number, encoding="ASCII")

def pad(number, blocksize):
	number = number % blocksize
	if number == 0:
		return bytes(0)
	else:
		return bytes(blocksize - number)

def pad_file(filename, blocksize, emptyerrormsg):
	s = os.stat(filename)
	if s.st_size == 0:
		os.unlink(filename)
		raise Exception(emptyerrormsg)
	# extend it to multiples of blocksize so it can be imported as block devices
	if s.st_size % blocksize != 0:
		fd = open(filename, "ab")
		fd.write(pad(s.st_size, blocksize))
		fd.close()

##############################################################################

class Cpio:
	"""q&d newc cpio generator"""
	filenameencoding = "utf-8"
	def __init__(self):
		self.seq = 0
		self.byteswritten = 0
		self.seen_directories = set([""])
	def output(self, data, expectedlength = None):
		if data is None:
			return
		if isinstance(data, list):
			data = b"".join(data)
		assert(isinstance(data, bytes))
		length = len(data);
		if expectedlength is not None:
			assert (expectedlength == length)
		if length == 0:
			return
		self.byteswritten += length
		self.write(data)

	def adddirectoryifneededof(self, filename):
		dn, bn = filename.rsplit("/", 1)
		self.adddirectoryifneeded(dn)
	def adddirectoryifneeded(self, dn):
		if dn in self.seen_directories:
			return
		self.adddirectoryifneededof(dn)
		self.adddirectory(dn)

	def addfile(self, filename, filesize, content, mode=0o0100644, uid=0, gid=0, nlink=1, mtime=0, devmajor=0, devminor=0, rdevmajor=0, rdevminor=0, mkdirhier=True):
		if mkdirhier:
			self.adddirectoryifneededof(filename)
		self.seq = self.seq + 1
		if isinstance(filename, bytes):
			binaryname = filename
		else:
			binaryname = bytes(filename,
				encoding=self.filenameencoding)
		bytestream = [ b"070701", o8(self.seq), o8(mode),
				o8(uid), o8(gid), o8(nlink),
				o8(mtime), o8(filesize),
				o8(devmajor), o8(devminor),
				o8(rdevmajor), o8(rdevminor),
				o8(len(binaryname) + 1), o8(0), binaryname,
				bytes(1), pad(len(binaryname) + 3, 4)]
		self.output(bytestream)
		self.output(content, expectedlength = filesize)
		self.output(pad(filesize, 4))
	def close(self):
		self.addfile(b"TRAILER!!!", 0, None, mkdirhier=False)
		self.output(pad(self.byteswritten, 512))
	def adddirectory(self, filename, perms=0o755, uid=0, gid=0, nlink=2, mtime=0):
		self.addfile(filename, 0, None, mode=perms|0o0040000,
				uid=uid, gid=gid, nlink=nlink, mtime=mtime)
		self.seen_directories.add(filename)
	def addchardevice(self, filename, major, minor, perms=0o644, uid=0, gid=0, nlink=1, mtime=0):
		self.addfile(filename, 0, None, mode=perms|0o0020000,
				uid=uid, gid=gid, nlink=nlink, mtime=mtime,
				rdevmajor=major, rdevminor=minor)
	def addregularfile(self, filename, filesize, content, perms=0o644, uid=0, gid=0, nlink=1, mtime=0):
		self.addfile(filename, filesize, content, mode=perms|0o0100000,
				 uid=uid, gid=gid, nlink=nlink, mtime=mtime)
	def addfilefromstring(self, filename, content, perms=0x644, uid=0, gid=0, mtime=0, encoding="utf-8"):
		content = bytes(content, encoding)
		self.addregularfile(filename, len(content), content,
				perms=perms, uid=uid, gid=gid)
	def addfilefromfs(self, filename, fromfile=None, perms=None, uid=None, gid=None, mtime=0):
		if fromfile is None:
			fromfile = filename
		s = os.stat(fromfile)
		if stat.S_ISREG(s.st_mode):
			self.addregularfile(filename, s.st_size,
				open(fromfile, "rb").read(),
				perms = stat.S_IMODE(s.st_mode)
					if (perms is None) else perms,
				uid = s.st_uid if (uid is None) else uid,
				gid = s.st_gid if (gid is None) else gid)
		else:
			raise Exception("Not yet supported file type of '%s'" % fromfile)
	def addsymlink(self, filename, target, perms=0o777, uid=0, gid=0, nlink=1, mtime=0):
		if not isinstance(target, bytes):
			target = bytes(target, encoding=self.filenameencoding)
		self.addfile(filename, len(target), target,
				mode=perms|0o0120000, nlink=nlink,
				uid=uid, gid=gid, mtime=mtime)

class CpioToFile(Cpio):
	def __init__(self, filename, **opts):
		super().__init__(**opts)
		self.filename = filename
		self.fd = open(filename, "wb")
		self.write = self.fd.write
	def close(self):
		super().close()
		self.fd.close()

class CpioToGz(Cpio):
	def __init__(self, filename, **opts):
		super().__init__(**opts)
		self.filename = filename
		self.fd = open(filename, "wb")
		self.gzprocess = subprocess.Popen(["gzip", "-n", "-9"],
			stdin=subprocess.PIPE,
			stdout=self.fd, stderr=None,
			close_fds=True)
		self.fd.close()
		self.write = self.gzprocess.stdin.write
	def close(self):
		super().close()
		self.gzprocess.stdin.close()
		if self.gzprocess.wait() != 0:
			raise Exception("Error executing gzip!")

##############################################################################

class InitRamFsPlain(Cpio):
	"""q&d initramfs generator"""

	def __init__(self, kernel=None):
		super().__init__()
		self.kernel=kernel
		self.modulestoload = []

	def addlinuxfile(self, filename):
		""" add a file from the kernel
		    (must be installed, later perhaps out of .deb)
		"""
		self.addfilefromfs(filename)

	def addmodule(self, module, tree="kernel", load=True):
		fn = "/lib/modules/%s/%s/%s" % (self.kernel, tree, module)
		self.addlinuxfile(fn)
		if load:
			self.modulestoload.append(fn)

	def finish(self, command):
		self.adddirectoryifneeded("/proc")
		self.adddirectoryifneeded("/run")
		self.adddirectoryifneeded("/sys")
		self.adddirectoryifneeded("/dev")
		self.addchardevice("/dev/null", major=1, minor=3, perms=0o666)
		self.addchardevice("/dev/console", major=5, minor=1, perms=0o600)
		self.addchardevice("/dev/ttyS0", major=4, minor=64, perms=0o666)
		self.addchardevice("/dev/ttyS1", major=4, minor=65, perms=0o666)
		self.addfilefromfs("/bin/busybox")
		self.addsymlink("/bin/sh", "busybox")
		# make busybox work without /proc mounted (so that /proc can be mounted and the shutdown works)
		self.addsymlink("/proc/self/exe", "/bin/busybox")
		self.addsymlink("/etc/mtab", "/proc/mounts")
		# TODO: move those templates to files or something like that
		# the init script:
		self.addfilefromstring("/init", """#! /bin/sh
echo "initramfs started..."
if test -n "${trace_early:+set}" ; then
	outershell() {
		echo "Starting debug shell because of an error in /init"
		PS1='initramfs:\w \$ ' setsid cttyhack sh -i
		/sbin/shutdown
	}
	trap outershell EXIT
	set -x
else
	trap /sbin/shutdown EXIT
fi
set -e
hostname "$hostname"
# TODO: update /etc/hosts?
mount -t proc -o nodev,nosuid,noexec proc /proc
mount -t sysfs -o nodev,nosuid,noexec sysfs /sys
mount -t devtmpfs -o nosuid,noexec dev /dev
export PATH=/bin:/sbin
echo "loading modules..."
""" + "\n".join(["insmod %s" % m for m in self.modulestoload]) + """
trap - EXIT
# some more predictable names for the virtio ports
mkdir /dev/virtio
for vc in /sys/class/virtio-ports/* ; do
	vcname="$(cat "$vc/name" 2> /dev/null || true)"
	vcdev="$(cat "$vc/dev" || true)"
	if test -n "$vcname" && test -n "$vcdev" ; then
		mknod "/dev/virtio/${vcname}" c "${vcdev%:*}" "${vcdev##*:}"
	fi
done
if test -n "${early_shell:+set}" ; then
	echo "Starting shell because early_shell is set..."
	PS1='initramfs:\w \$ ' setsid cttyhack sh -i
fi
echo "switching to init"
exec init
""", perms=0x755)
		# the inittab
		self.addfilefromstring("/etc/inittab", """# inittab so we can have additional ttys
::respawn:/sbin/run_once_or_die
l0:0:wait:/sbin/shutdown
hvc0::askfirst:-/bin/sh
hvc1::askfirst:-/bin/sh
""")
		# the main respawner (so failures get notices)
		self.addfilefromstring("/sbin/run_once_or_die", """#! /bin/sh
if test -f /run/already_started ; then
	if test -n "${{late_shell:+set}}" ; then
		echo "Starting shell because late_shell is set..."
		PS1='initramfs:\w \$ ' setsid cttyhack sh -i
	fi
	while test -f /run/waitwithshutdown ; do
		echo "waiting for /run/waitwithshutdown to be deleted"
		sleep 5
	done
	# always power down
	/sbin/shutdown
fi
touch /run/already_started
set -e
echo 'starting {command}...'
{command}
""".format(command=command), perms=0o755)
		self.addfilefromstring("/sbin/shutdown", """#! /bin/sh
echo "Shutting down VM..."
umount -l /dev
umount -a
poweroff
poweroff -f
kill -KILL 1
""", perms=0x755)
		# some bells and whistles
		self.addfilefromstring("/sbin/disallowshutdown", """#! /bin/sh
touch /run/waitwithshutdown
""", perms=0o755)
		self.addfilefromstring("/sbin/allowshutdown", """#! /bin/sh
rm /run/waitwithshutdown ||
echo "Cannot remove lock. Not locked with disallowshutdown?"
""", perms=0o755)
		self.close()

##############################################################################

class InitRamFs(CpioToGz, InitRamFsPlain):
	pass

##############################################################################

def generateinitramfs(initramfsname, kernel):
	p = InitRamFs(initramfsname, kernel=kernel)
	p.addmodule("drivers/virtio/virtio.ko")
	p.addmodule("drivers/virtio/virtio_ring.ko")
	p.addmodule("drivers/char/virtio_console.ko")
	p.addmodule("drivers/virtio/virtio_pci.ko")
	p.addmodule("drivers/virtio/virtio_balloon.ko")
	p.addmodule("drivers/block/virtio_blk.ko")
	p.addmodule("net/9p/9pnet.ko")
	p.addmodule("net/9p/9pnet_virtio.ko")
	p.addmodule("fs/fscache/fscache.ko")
	p.addmodule("fs/9p/9p.ko")

	p.adddirectory("/target")
	p.adddirectory("/srv/mirror")
	p.addfilefromstring("/bin/bdpivm_main", """#! /bin/sh
if test -n "${trace_main:+set}" ; then set -x ; fi
set -e
set -o pipefail
if test -n $bdpivm_lo ; then ip address add 127.0.0.1/8 dev lo ; ip address add ::1 dev lo ; fi
mount -t 9p -o ro,nodev,nosuid,noexec,trans=virtio,version=9p2000.L mirror /srv/mirror
export DEBIAN_FRONTEND=noninteractive
export LC_ALL=C
umountifmounted() { if grep -q "^[^ ]* $1 " /proc/mounts; then umount -l "$1" ; fi ; }
if test x"${create:-no}" = x"yes" ; then
	echo "unpacking debootstrap tarball..."
	if test -n "${list_commands:+set}" ; then v=v ; else v="" ; fi
	cd / && tar -x${v}f /dev/vda
	mkdir -p /root
	echo "unpacking basetar creation scripts tarball..."
	cd /root && tar -xz${v}f /dev/vdb
	/root/commands
elif test x"${unpack}" != x"no" ; then
	echo unpacking tar
	if test -n "${list_basetgz:+set}" ; then v=v ; else v="" ; fi
	cd /target && tar -xz${v}f /dev/vda
	echo mounting stuff
	mount -o nodev,nosuid,noexec -t proc proc /target/proc
	mkdir -p /target/srv/mirror
	mount -o bind /srv/mirror /target/srv/mirror
	mkdir -p /target/srv/buildd
	mount -t tmpfs -o nosuid,nodev"${mount_buildd_size:+,size=}${mount_buildd_size}" tmpfs /target/srv/buildd
	mkdir -p /target/tmp
	mount -t tmpfs -o "${tmpopt}${mount_tmp_size:+,size=}${mount_tmp_size}" tmpfs /target/tmp
	mkdir -p /target/run
	mount -t tmpfs -o noexec,nosuid,nodev,mode=0755"${mount_run_size:+,size=}${mount_run_size}" tmpfs /target/run
	mkdir /target/run/lock
	chmod 04777 /target/run/lock
	mkdir /target/run/shm
	chmod 04777 /target/run/shm
	if test -e "/dev/vdb" ; then
		if test -n "${list_commands:+set}" ; then v=v ; else v="" ; fi
		mkdir -p /target/srv/buildd/input
		(cd /target/srv/buildd/input && tar --no-same-owner --no-same-permissions -xz${v}f /dev/vdb) || { echo "Failed to extract input!" ; exit 1 ; }
		if test -e /dev/virtio/logfile ; then
			chroot /target su - -c /srv/buildd/input/commands 2>&1 | tee -a /dev/virtio/logfile || { echo "Commands failed with exit code ${?}!" ; exit 1 ; }
		else
			setsid cttyhack chroot /target su - -c /srv/buildd/input/commands || { echo "Commands failed with exit code ${?}!" ; exit 1 ; }
		fi
	else
		echo "Welcome to the chroot inside the VM."
		setsid cttyhack chroot /target su -
	fi
else
	echo "Welcome to a shell inside the VM."
	setsid cttyhack su -
fi
umountifmounted /target/proc
umountifmounted /target/tmp
umountifmounted /target/run
umountifmounted /target/srv/mirror
if test -d /target/srv/buildd/output && test -e /dev/virtio/output ; then
	if test -e /dev/virtio/logfile ; then
		(cd /target/srv/buildd/output && sha256sum *) >> /dev/virtio/logfile
	fi
	echo "Creating results tar..."
	(cd /target/srv/buildd/output && tar -czf /dev/virtio/output .)
	echo results exported
fi
umountifmounted /target/srv/buildd
if test -e /dev/virtio/basetgzout ; then
	rm -f /target/etc/apt/sources.list.d/bdpivm.list
	echo "Creating new base tar and compressing it..."
	(cd /target && tar -czf /dev/virtio/basetgzout .)
	echo new base tar gz exported
fi
""", perms=0o755)

	# example how to add other programs:
	#p.addfilefromfs("/lib64/ld-linux-x86-64.so.2", fromfile="/lib/x86_64-linux-gnu/ld-2.17.so")
	#p.addfilefromfs("/lib/libtinfo.so.5", "/lib/x86_64-linux-gnu/libtinfo.so.5")
	#p.addfilefromfs("/lib/libdl.so.2", "/lib/x86_64-linux-gnu/libdl.so.2")
	#p.addfilefromfs("/lib/libc.so.6", "/lib/x86_64-linux-gnu/libc.so.6")
	#p.addfilefromfs("/bin/bash")
	p.finish("/bin/bdpivm_main")

parser = argparse.ArgumentParser(description="build debian packages in a virtual machine")
parser_modi_group = parser.add_argument_group("modi", "primary mode of operation")
parser_modi_group.add_argument("--login", dest='mode', action='store_const', const='login', help="login")
parser_modi_group.add_argument("--build", dest='mode', action='store_const', const='build', help="build a package")
parser_modi_group.add_argument("--create", dest='mode', action='store_const', const='create', help="create the basetar file (implies storebasetar)")
parser_modi_group.add_argument("--update", dest='mode', action='store_const', const='update', help="update the basetar file (implies storebasetar)")
parser_bld_group = parser.add_argument_group("options triggering build and passed to dpkg-buildpackage")
parser_bld_group.add_argument("--no-unreleased-version-mangle", dest='unreleasedmangle', action='store_false', default='true', help="do not mangle version of UNRELEASED builds")
parser_bld_group.add_argument("-B", dest='buildmode', action='store_const', const='arch', help="pass -B to dpkg-buildpackage")
parser_bld_group.add_argument("-b", dest='buildmode', action='store_const', const='indep', help="pass -b to dpkg-buildpackage")
parser_bld_group.add_argument("-S", dest='buildmode', action='store_const', const='source', help="pass -S to dpkg-buildpackage")
parser_opt_group = parser.add_argument_group("general options")
parser_opt_group.add_argument("--storebasetar", dest='storebasetar', action='store_true', default=False, help="store the basetgz once everything is done")
parser_opt_group.add_argument("--ui", dest='access', type=str, default="stdio", help="how to access", choices=["stdio", "vc"])
parser_opt_group.add_argument("--allow-devices", dest='allow_devices', action='store_true', default=False, help="allow qemu devices (network, cdrom, vga, ...)")
parser_opt_group.add_argument("--executable-tmp", dest='executable_tmp', action='store_true', default=False, help="mount /tmp executable inside the chroot")
parser_opt_group.add_argument("--suite", dest='suite', type=str, default=None, help="suite to use with --create")
# TODO: also support pubring elsewhere...
parser_opt_group.add_argument("--pubring", dest='pubring', type=str, default="pubring.gpg", help="File with pubring relative to mirrordir")
parser_opt_group.add_argument("--debarch", dest='debarch', type=str, default=None, help="Debian architecture to use with --create")
parser_opt_group.add_argument("--qemuarch", dest='qemuarch', type=str, default=None, help="Qemu architecture")
parser_opt_group.add_argument("--basetar", dest='basetar', type=str, default=None, help="the base.tgz file")
parser_opt_group.add_argument("--extrapackage", dest='extrapackages', action='append', type=str, default=[])
parser_opt_group.add_argument("--mirrordir", dest='outermirrordir', type=str, default="/srv/mirror", help="directory to made accessible inside the VM as /srv/mirror")
parser_opt_group.add_argument("--kernelversion", default=platform.uname().release, dest='kernelversion', type=str, help="kernel version to use (must yet be installed)")
parser_ops_group = parser.add_argument_group("more obscure options")
parser_ops_group.add_argument("--tempdir", dest='tmpdir', type=str, default=None, help="directory for temporary files")
parser_ops_group.add_argument("--cachdir", dest='cachedir', type=str, default=None, help="directory for supplemental files")
parser_ops_group.add_argument("--resultdir", dest='resultdir', type=str, default=None, help="directory where results are stored (default: in a subdirectory of --resultbasedir)")
parser_ops_group.add_argument("--resultbasedir", dest='resultbasedir', type=str, default=None, help="directory where results are stored in a subdir (unless --resultdir is given)")
parser_ops_group.add_argument("--tmpbasetar", dest='tmpbasetar', type=str, default=None, help="temporary new base.tgz file (for storebasetar)")
parser_ops_group.add_argument("--initramfs", dest='initramfs', type=str, default=None, help="initramfs to generate/use")
parser_dbg_group = parser.add_argument_group("debug options")
debug_cmdline_flags = ["early_shell", "late_shell", "trace_early", "trace_main", "list_basetgz", "list_commands"]
debug_direct_flags = ["trace_commands", "kernel", "qemucmdline", "keepinputtar", "afterbuildshell"]
parser_dbg_group.add_argument("--debug", action="append", dest='debugopts', choices=debug_cmdline_flags + debug_direct_flags, default=[])
parser.add_argument("dscfile", nargs="?", type=str, help="dsc file to build (implies --build)")
options = parser.parse_args()
debugopts = set(options.debugopts)

if options.debarch is None and options.qemuarch is None:
	options.qemuarch = platform.uname().machine
	if options.mode == "create":
		p = subprocess.Popen(["dpkg", "--print-architecture"],
			stdin=subprocess.DEVNULL,
			stdout=subprocess.PIPE)
		options.debarch = p.communicate()[0].decode("UTF-8").strip()

if options.mode is None and ( options.dscfile is not None or options.buildmode is not None):
	options.mode = "build"
if options.mode != "build" and options.dscfile is not None:
	raise Exception("Cannot combine file to build ('%s') with --%s" % (options.dscfile, options.mode))
if options.mode != "build" and options.buildmode is not None:
	raise Exception("Cannot combine -B -b or -S with --%s" % (options.dscfile, options.mode))
if options.mode == "build" and not options.buildmode:
		options.buildmode = "full"
if options.mode is None:
	raise Exception("Nothing to do given")

if options.mode != "create":
	if options.suite is not None:
		raise Exception("--suite is currently only possible with --create")
else:
	if options.suite is None:
		options.suite = "sid"

if options.mode == "update" or options.mode == "create":
	options.storebasetar = True

if options.mode == "create" and options.debarch is None:
	raise Exception("Need --debarch with --create if --qemuarch is used!")

if options.tmpbasetar is not None and not options.storebasetar:
	print("Hint: --tempbasetar is useless without --storebasetgz")

class Files():
	def mkdirhier(self, dir):
		if os.path.isdir(dir):
			return
		dn = os.path.dirname(dir)
		self.mkdirhier(dn)
		os.mkdir(dir)
	def dir(self, fromcmdline, default=None, env=None):
		if fromcmdline is not None:
			dir = fromcmdline
		elif default is not None:
			if env is not None:
				dir = default % os.environ[env]
			else:
				dir = default
		else:
			return tempfile.mkdtemp(prefix="bdpivm")
		self.mkdirhier(dir)
		return dir
	def __init__(self, options):
		t = time.time()
		self.id = str(int(t))
		self.date = time.strftime("%Y-%m-%d", time.gmtime(t))
		self.packagename = None

		self.outlogfile = None
		self.cachedir = self.dir(options.cachedir, "%s/.bdpivm/cache", "HOME")
		self.tmpdir = self.dir(options.tmpdir)
		if options.resultdir is None:
			self.resultbasedir = self.dir(options.resultbasedir, "%s/.bdpivm/results", "HOME")
		self.outputtgz = "%s/output-%s.tgz" % (self.tmpdir, self.id)
		if os.path.exists(self.outputtgz):
			os.unlink(self.outputtgz)
		self.debootstraptar = "%s/debootstrap_%s.tar" % (self.cachedir, options.qemuarch)
		if options.basetar is None:
			# TODO: in some special directory
			self.basetar = "%s/base_%s.tgz" % (self.cachedir, options.qemuarch)
		else:
			self.basetar = options.basetar
		if options.tmpbasetar is None:
			self.tmpbasetar = "%s.new.tmp" % self.basetar
		else:
			self.tmpbasetar = options.tmpbasetar
		if options.initramfs is None:
			self.initramfs = "%s/initramfs-%s.gz" % (self.cachedir, options.kernelversion)
		else:
			self.initramfs = options.initramfs

	def setpackagename(self, packagename):
		self.packagename = packagename

	def prepareoutlogfile(self, mode):
		if self.packagename:
			description = self.packagename
		else:
			description = mode
		self.outlogfile = "%s/%s-%s-%s.log" % (self.resultbasedir,
			description, self.date, self.id)
		if os.path.exists(self.outlogfile):
			os.unlink(self.outlogfile)

	def createresultdir(self):
		if self.packagename:
			resultdir = "%s/%s-%s-%s" % (self.resultbasedir,
				self.packagename, self.date, self.id)
		else:
			resultdir = "%s/%s-%s" % (self.resultbasedir, self.date,
				 self.id)
		os.mkdir(resultdir)
		if self.outlogfile:
			newoutlog = "%s/build.log" % resultdir
			os.rename(self.outlogfile, newoutlog)
			self.outlogfile = newoutlog
		return resultdir

	def start_input(self):
		self.inputtgz = "%s/input-%s.tgz" % (self.tmpdir, self.id)
		self.inputdir = "%s/input-%s" % (self.tmpdir, self.id)
		# error if it already exists to avoid overwriting one in progress
		if os.path.exists(self.inputtgz):
			raise Exception("%s already exists" % self.inputtgz)
		os.mkdir(self.inputdir)
		self.inputfiles = []
	def newsymlink(self, filename, target, directory=None):
		if directory is None:
			directory=self.inputdir
			self.inputfiles.append(filename)
		if not os.path.exists(directory):
			os.mkdir(directory)
		os.symlink(os.path.abspath(target),
				"%s/%s" % (directory, filename))
	def newfilename(self, filename, directory=None):
		if directory is None:
			directory=self.inputdir
			self.inputfiles.append(filename)
		return "%s/%s" % (directory, filename)
	def newfile(self, filename, content, directory=None, mode=0o755):
		if directory is None:
			directory=self.inputdir
			self.inputfiles.append(filename)
		if not os.path.exists(directory):
			os.mkdir(directory)
		fullfilename = "%s/%s" % (directory, filename)
		fd = open(fullfilename, "x", encoding="UTF-8")
		os.fchmod(fd.fileno(), mode)
		fd.write(content)
		fd.close()
	def createtar(self, filename, directory, arguments=["."], compressed=True):
		if subprocess.call(["tar", "-C", directory, "-czhf" if compressed else "-chf", filename] + arguments) != 0:
			try:
				os.unlink(filename)
			except FileNotFoundError:
				pass
			raise Exception("Failed to create tarfile %s" % filename)
# 	def concattar(self, filename, *files):
# 		print(["tar", "--concat", "-f"] + list(files))
# 		if subprocess.call(["tar", "--concat","-f", filename] + list(files)) != 0:
# 			try:
# 				os.unlink(filename)
# 			except FileNotFoundError:
# 				pass
# 			raise Exception("Failed to create tarfile %s" % filename)
	def createinputtar(self):
		self.createtar(self.inputtgz, self.inputdir)
		pad_file(self.inputtgz, 512, emptyerrormsg="Empty tar generated?? Something is broken.")
		for f in self.inputfiles:
			os.unlink("%s/%s" % (self.inputdir, f))
		os.rmdir(self.inputdir)
		self.inputfiles = None
#	def createdebootstraptar(self):
#		self.createtar(self.debootstraptar, "/", ["/usr/share/debootstrap", "/usr/sbin/debootstrap"], compressed=False)
#		self.concattar(self.debootstraptar, "libc-udeb.tar")
#		self.concattar(self.debootstraptar, "bootstrap-base-udeb.tar")
#		self.concattar(self.debootstraptar, "zlib-udeb.tar")
#		self.concattar(self.debootstraptar, "gpgv-udeb.tar")
#		pad_file(self.debootstraptar, 512, emptyerrormsg="Empty debootstrap tar generated?? Something is broken.")

files = Files(options)

def readdsc(filename):
	fd = open(filename, "rt", encoding="UTF-8")
	content = fd.read()
	fd.close()
	if content.startswith("-----BEGIN"):
		p = subprocess.Popen(["gpg", "--decrypt"],
			stdin=subprocess.PIPE,
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE)
		return p.communicate(bytes(content, encoding="UTF-8"))[0].decode("UTF-8")
	else:
		return content

def callorfail(*args, **opts):
	if subprocess.call(args, **opts) != 0:
		raise(Exception("Failure running %s!" % " ".join("'%s'" % a for a in args)))

def generate_commands():
	files.start_input()
	# those
	commands=["#! /bin/sh",
		"set -x" if "trace_commands" in debugopts else "",
		"set -e",
		"export LC_ALL=C",
		"export DEBIAN_FRONTEND=noninteractive",
	]
	if options.mode == "create":
		commands.extend([
			"ln -s busybox /bin/md5sum",
			"ln -s busybox /bin/sha1sum",
			"ln -s busybox /bin/sha256sum",
			# Workaround for debootstrap no longer working with bootstrap-base otherwise...
			"sed -e 's/^DEBOOTSTRAP_CHECKSUM_FIELD=.*/DEBOOTSTRAP_CHECKSUM_FIELD=MD5SUM/' -i /usr/sbin/debootstrap",
			"ln -s lib /lib64 || true",
			"debootstrap --extractor ar --keyring=/srv/mirror/{pubring} --arch={debarch} --variant=buildd {suite} /target file:/srv/mirror".format(suite=options.suite, debarch=options.debarch, pubring=options.pubring),
			# No recommends on a buildd
			"mkdir -p /target/apt/apt.conf.d",
			"echo 'APT::Install-Recommends \"false\";' > /target/etc/apt/apt.conf.d/norecommends",
			# don't start anything in there:
			'if ! test -f /target/usr/sbin/policy-rc.d ; then printf \'#!/bin/sh\nwhile test "$1" != "${1#-}" ; do shift ; done ; case "$1" in makedev|x11-common) exit 0 ;; esac ; exit 101\n\' > /target/usr/sbin/policy-rc.d ; chmod a+x /target/usr/sbin/policy-rc.d ; fi',
			# Work around debootstrap only trusting http:// mirrors
			"echo 'deb file:///srv/mirror {suite} main' > /target/etc/apt/sources.list".format(suite=options.suite),
			"mkdir -p /target/srv/mirror",
			"mount --bind /srv/mirror /target/srv/mirror",
			"cp /srv/mirror/%s /target/etc/apt/trusted.gpg.d/localmirror.gpg" % options.pubring,
			"chroot /target apt-get update",
			"chroot /target apt-get -y install build-essential dpkg-dev deborphan debhelper",
			"chroot /target apt-get -y --purge autoremove",
			"chroot /target apt-get clean",
		])
	elif options.mode == "update":
		commands.extend([
			"apt-get -q update",
			"apt-get -y dist-upgrade",
			"apt-get -y --purge autoremove",
			"apt-get -y install build-essential dpkg-dev deborphan debhelper",
			"apt-get clean",
		])
	elif options.mode == "build":
		if options.dscfile is not None:
			dscbasename = os.path.basename(options.dscfile)
			dsccontent = readdsc(options.dscfile)
			files.newsymlink(dscbasename, options.dscfile)
			packagename = dscbasename.split("_")[0]
			dscfiles = []
			l = iter(dsccontent.split("\n"))
			while not next(l).startswith("Files:"):
				pass
			line = next(l, "End")
			while line.startswith(" "):
				dscfiles.append(line.split(" ")[3])
				line = next(l, "End")
			unpackcommands = [
				'cd /srv/buildd/builddir && dpkg-source -x ../input/*.dsc',
			]
			for d in dscfiles:
				files.newsymlink(d, "%s/%s" % (os.path.dirname(options.dscfile),d))
		elif os.path.exists("debian/control"):
			dsclines= open("debian/control", encoding="UTF-8").read().split("\n")
			if dsclines.index("") > 0:
				dsclines = dsclines[:dsclines.index("")]
			packagename = None
			for l in dsclines:
				if l.startswith("Source:"):
					packagename = l.split(":", 1)[1].strip()
			if packagename is None:
				raise(Exception("No Packagename found in debian/control"))
			dsccontent = "\n".join(dsclines)
			if os.path.exists(".git"):
				callorfail("git", "archive", "-o", files.newfilename("sourcedir.tar.gz"), "--prefix=sourcedir/", "HEAD")
			else:
				callorfail("tar", "-czf", files.newfilename("sourcedir.tar.gz"), "--transform=s#^\./#sourcedir/#", ".")
			if os.path.exists("debian/.git-dpm"):
				origtarball = open("debian/.git-dpm", encoding="UTF-8").read().split("\n")[5]
			else:
				p = subprocess.Popen(["dpkg-parsechangelog", "-SVersion"],
					stdin=subprocess.DEVNULL,
					stdout=subprocess.PIPE)
				version = p.communicate()[0].decode("UTF-8").strip()
				if version == "":
					raise(Exception("No yet implemented way to calculate the .orig.tar filename"))
				uversion = version.rsplit("-", 1)[0].split(":", 1)[-1]
				if uversion == version:
					# Native
					origtarball = None
				else:
					version = uversion
					origtarball = "%s_%s.orig.tar" % (packagename, version)
					if os.path.exists("../" + origtarball + ".xz"):
						origtarball = origtarball + ".xz"
					elif os.path.exists("../" + origtarball + ".bz2"):
						origtarball = origtarball + ".bz22"
					elif os.path.exists("../" + origtarball + ".gz"):
						origtarball = origtarball + ".gz"
					elif not os.path.exists("../" + origtarball):
						origtarball = origtarball + ".gz"
			dscfiles = []
			unpackcommands = []
			if origtarball is not None:
				if not os.path.exists("../" + origtarball) and os.path.exists(".git"):
					callorfail("pristine-tar", "checkout", "../%s" % origtarball)
				files.newsymlink(origtarball, os.path.abspath("../%s" % origtarball ))
				if os.path.exists("../%s.asc" % origtarball):
					files.newsymlink(origtarball + ".asc", os.path.abspath("../%s.asc" % origtarball ))
				unpackcommands.append('cp /srv/buildd/input/*.orig.tar* /srv/buildd/builddir/')
			dscfiles = []
			unpackcommands.extend([
				'cd /srv/buildd/builddir && tar -xzf ../input/sourcedir.tar.gz',
				'cd /srv/buildd/builddir/sourcedir && sed -e "1s/) *UNRELEASED/~%s&/" -i debian/changelog' % files.id if options.unreleasedmangle != False else None,
				'cd /srv/buildd/builddir/sourcedir && dpkg-buildpackage -rfakeroot -us -uc -S',
				'cd /srv/buildd/builddir/sourcedir && dpkg-buildpackage -rfakeroot -us -uc -S',
				'cd /srv/buildd/builddir && rm *.changes',
				'cd /srv/buildd/builddir && dpkg-source -x *.dsc',
			])
		else:
			raise(Exception("Need either a dsc file given or be called from a repository"))
		files.setpackagename(packagename)
		files.newfile("sources", ("Package: %s\n" % packagename) +
					dsccontent)
		if options.buildmode == "arch":
			archonlyflag = "--arch-only"
			archonlyopt= "-B"
		elif options.buildmode == "indep":
			archonlyflag = ""
			archonlyopt= "-b"
		elif options.buildmode == "source":
			archonlyflag = ""
			archonlyopt= "-S"
		elif options.buildmode == "full":
			archonlyflag = ""
			archonlyopt= ""
		commands.extend([
			"mkdir -p /srv/buildd/repo/dists/buildd/this/source",
			# """for n in /srv/buildd/input/*.dsc ; do if grep -q '^-----BEGIN' "$n" ; then gpg --decrypt "$n" ; else cat $n ; fi ; echo ; done | sed -e "s/^Source:/Package:/" >/srv/buildd/repo/dists/buildd/this/source/Sources""",
			"cp /srv/buildd/input/sources /srv/buildd/repo/dists/buildd/this/source/Sources",
			"mkdir -p /etc/apt/sources.list.d",
			"echo 'deb-src [ trusted=yes check-date=no ] file:///srv/buildd/repo buildd this' > /etc/apt/sources.list.d/bdpivm.list",
			"apt-get -q update",
			"apt-get -q install -y fakeroot %s" % (" ".join(options.extrapackages)),
			"""apt-get -y build-dep --only-source %s "%s" """ % (archonlyflag, packagename),
			"# too bad one cannot apt-get --purge autoremove after deinstalling apt:",
			"dpkg -P debian-archive-keyring apt",
			"dpkg -P gnupg || true",
			"""while true ; do canberemoved="$(deborphan)" ; if test -z "$canberemoved" ; then break ; fi ; dpkg -P ${canberemoved} ; done""",
			"dpkg -P deborphan",
			"umount /srv/mirror",
			"mkdir /srv/buildd/donottouch",
			"echo 'builder:x:12345:12345::/srv/buildd/donottouch:/bin/sh' >> /etc/passwd",
			"echo 'builder:x:12345:' >> /etc/group",
			"echo 'builder:*:1:0:999999:7:::' >> /etc/shadow",
			"echo 'builder:*::' >> /etc/gshadow",
			"install -d -o builder -g builder -m 0700 /srv/buildd/builddir",
		])
		def asbuilder(u):
			if u.find("'") >= 0:
				raise Exception("\"'\" in unpack-command not supported")
			return "su - builder -c '%s'" % u
		commands.extend(asbuilder(u) for u in unpackcommands if u)
		commands.extend([
			"su - builder -c 'cd /srv/buildd/builddir/%s-* && dpkg-buildpackage -rfakeroot -us -uc -j2 %s -I\".git*\"'" % (packagename, archonlyopt),
			"mkdir -p /srv/buildd/output",
			# TODO: instead look into generated .changes file:
			"find /srv/buildd/builddir -maxdepth 1 -type f -exec cp {} /srv/buildd/output \;",

		])
		if "afterbuildshell" in debugopts:
			commands.append("echo '### Starting after-build-shell'")
			commands.append("echo '### To get something out of the vm, copy it to /srv/buildd/output'")
			commands.append("setsid su - builder -c 'cd /srv/buildd/builddir/sourcedir && setsid bash -i' >&0 2>&0")
	else:
		raise Exception("unexpected mode '%s'. How did I get here?" % options.mode)
	files.newfile("commands", "\n".join(commands))
	files.createinputtar()


if not os.path.exists(files.initramfs):
	print("Generating '%s'... " % files.initramfs, end="")
	generateinitramfs(files.initramfs, kernel=options.kernelversion)
	print("done")

if options.mode == "create":
	if not os.path.exists(files.debootstraptar):
		print("""
Error: --create given but there is not yet a {bootstraptar}.

In order to --create a new base.tar you need to create a tarball
with debootstrap and it's dependencies for the architecture you are
targeting.

The following should work:

* Get debootstrap-udeb_*_all.udeb, bootstrap-base_*_{debarch}.udeb,
gpgv-udeb_*_{debarch}.udeb, zlib1g-udeb_*_{debarch}.udeb and
libc*_*_{debarch}.udeb (should be found on any mirror).

* Use dpkg-deb --fsys-tarfile on each of those to get a .tar file.

* use tar -Af firstfile.tar secondfile.tar
  for each of those to combine them to one tarfile.

* Store the result as '{bootstraptar}'.
""".format(bootstraptar=files.debootstraptar, debarch=options.debarch))
		raise SystemExit(1)
	firsttar = files.debootstraptar
else:
	if not os.path.exists(files.basetar):
		print("""
Error: There is no {basetar} yet.

Perhaps you can generate some with --create, or you might
be able to reuse a pbuilder basetar (after some minor modifications).
""".format(basetar=files.basetar))
		raise SystemExit(1)
	firsttar = files.basetar

# Needs to be padded so it can be used as block device.
pad_file(firsttar, 512, emptyerrormsg="Empty %s file???" % firsttar)


kernelcommandline=["console=ttyS0", "hostname=buildbot%s" % files.id, ]
if options.mode == "create":
	kernelcommandline.append("create=yes")
serialopts = []
# console is always ttyS0, the question is only where to access those:
if options.access == "stdio":
	serialopts = [
	"-nographic",
	"-chardev", "stdio,id=console",
	"-chardev", "pty,id=debugconsole1",
	"-chardev", "pty,id=debugconsole2",
	"-chardev", "pty,id=qemu_command_prompt",
	]
	prestartmsg="To connect to one of those the /dev/pts/X run e.g. screen /dev/pts/X and press return."
elif options.access == "vc":
	prestartmsg="use Ctrl-Alt-2 to switch to debug console, Ctrl-Alt-4 for qemu console"
	serialopts = [
	"-chardev", "vc,id=console",
	"-chardev", "vc,id=debugconsole1",
	"-chardev", "vc,id=debugconsole2",
	"-chardev", "vc,id=qemu_command_prompt",
	]
if options.executable_tmp:
	kernelcommandline.append("tmpopt=exec,nodev,nosuid")
else:
	kernelcommandline.append("tmpopt=noexec,nodev,nosuid")
if not "kernel" in debugopts:
	kernelcommandline.append("quiet")
for f in debug_cmdline_flags:
	if f in debugopts:
		kernelcommandline.append("%s=1" % f)

def commaescape(v):
	return v.replace(",", ",,")

arguments = ["qemu-system-%s" % options.qemuarch,
	"-enable-kvm", "-m", "8G", "-smp", "2",
]
if not options.allow_devices:
	arguments.extend(["-nodefaults",
	"-net", "none", "-vga", "none", "-parallel", "none",
])
arguments.extend([
	"-kernel", "/boot/vmlinuz-%s" % options.kernelversion,
	"-initrd", files.initramfs,
	"-append", " ".join(kernelcommandline),
	# way to import basetgz
	"-drive", "file=%s,if=virtio,media=disk,format=raw,cache=unsafe,readonly" % commaescape(firsttar),
	# way to import mirror:
	"-fsdev", "local,id=fsdev0,readonly,path=%s,security_model=none" % commaescape(options.outermirrordir),
	"-device", "virtio-9p-pci,fsdev=fsdev0,mount_tag=mirror,id=fs0",
	# beside networks or block devices, that seems to be the only
	# way to get output (like a new basetgz) sufficiently fast out
	# of the virtual machine:
	"-device", "virtio-serial",
	])
arguments.extend(serialopts)
arguments.extend([
	"-serial", "chardev:console",
	"-device", "virtconsole,chardev=debugconsole1",
	"-device", "virtconsole,chardev=debugconsole2",
	"-mon", "chardev=qemu_command_prompt",
	# some options for serial
	])
if options.storebasetar:
	arguments.extend([
		"-chardev", "file,id=basetgzout,path=%s" % commaescape(files.tmpbasetar) ,
		"-device", "virtserialport,chardev=basetgzout,name=basetgzout",
	])
if options.mode != "login":
	generate_commands()
	arguments.extend([
		"-drive", "file=%s,if=virtio,media=disk,format=raw,cache=unsafe,readonly" % commaescape(files.inputtgz),
	])
if options.mode != "login" and options.mode != "create":
	files.prepareoutlogfile(options.mode)
	arguments.extend([
		"-chardev", "file,id=output,path=%s" % commaescape(files.outputtgz) ,
		"-device", "virtserialport,chardev=output,name=output",
		"-chardev", "file,id=logfile,path=%s" % commaescape(files.outlogfile) ,
		"-device", "virtserialport,chardev=logfile,name=logfile",
	])
print(prestartmsg)
if "qemucmdline" in debugopts:
	print("Running %s" % " ".join("'%s'" % a for a in arguments))
subprocess.call(arguments)
if options.mode != "login" and not "keepinputtar" in debugopts:
	os.unlink(files.inputtgz)
if options.storebasetar:
	pad_file(files.tmpbasetar, 512, emptyerrormsg="Run resulting in empty new base tar file. Some error must have occured.")
	os.rename(files.tmpbasetar, files.basetar)
	print("Created new %s." % files.basetar)
if os.path.exists(files.outputtgz) and os.stat(files.outputtgz).st_size > 0:
	resultdir = files.createresultdir()
	print("Output of build stored in %s" % resultdir)
	subprocess.call(["tar", "-C", resultdir,
		"--no-xattrs", "--no-acls", "--no-selinux",
		"-k", "-m",
		"--no-recursion",
		"--no-same-permissions", "--no-same-owner",
		"--transform", 's#^\./##',
		"--transform", 's#[^a-zA-Z0-9._:~+-]#_#g',
		"-xvzf", files.outputtgz])
if files.outlogfile:
	print("A logfile can be found at %s" % files.outlogfile)
